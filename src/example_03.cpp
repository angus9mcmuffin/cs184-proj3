#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <sstream>

//include header file for glfw library so that we can use OpenGL
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <windows.h>

#ifdef _WIN32
static DWORD lastTime;
#else
static struct timeval lastTime;
#endif

#define PI 3.14159265 // Should be used from mathlib

using namespace std;
float epsilon = 0.00001;
int adapt = 0;
float step = 0;
vector<vector<vector<vector<float>>>> allPatches;
vector<vector<vector<float>>> allQuads;
vector<vector<vector<float>>> allTriangles;
vector<vector<vector<float>>> allNormals;
int rendered = 0;

/*
For UC Berkeley's CS184 Fall 2016 course, assignment 3 (Bezier surfaces)
*/

//****************************************************
// Global Variables
//****************************************************
GLfloat translation[3] = {0.0f, 0.0f, 0.0f};
GLfloat rotationX = 0.0f;
GLfloat rotationY = 0.0f;
GLfloat scaling = 1.0f;
bool auto_strech = false;
int Width_global = 400;
int Height_global = 400;
int Z_buffer_bit_depth = 128;

inline float sqr(float x) { return x*x; }


//****************************************************
// Simple init function
//****************************************************
void initializeRendering()
{
    glfwInit();

}


//****************************************************
// A routine to set a pixel by drawing a GL point.  This is not a
// general purpose routine as it assumes a lot of stuff specific to
// this example.
//****************************************************
void setPixel(float x, float y, GLfloat r, GLfloat g, GLfloat b) {
    glColor3f(r, g, b);
    glVertex2f(x+0.5, y+0.5);  // The 0.5 is to target pixel centers
    // Note: Need to check for gap bug on inst machines.
}

//****************************************************
// Keyboard inputs. Add things to match the spec!
//****************************************************
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  GLint params[2];
    switch (key) {

        case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
        case GLFW_KEY_Q: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
		case GLFW_KEY_EQUAL: if (action && mods == GLFW_MOD_SHIFT) scaling += 0.1f; break; 
		case GLFW_KEY_MINUS: scaling -= 0.1f; break;
        case GLFW_KEY_LEFT :
            if (action && mods == GLFW_MOD_SHIFT) {translation[0] -= 0.001f * Width_global; break;}
            else if (mods != GLFW_MOD_SHIFT) {rotationY -= 4; break;}
            break;
        case GLFW_KEY_RIGHT:
            if (action && mods == GLFW_MOD_SHIFT) {translation[0] += 0.001f * Width_global; break;}
            else if (mods != GLFW_MOD_SHIFT) {rotationY += 4; break;}
            break;
        case GLFW_KEY_UP   :
            if (action && mods == GLFW_MOD_SHIFT) {translation[1] += 0.001f * Height_global; break;}
            else if (mods != GLFW_MOD_SHIFT) {rotationX -= 4; break;}
            break;
        case GLFW_KEY_DOWN :
            if (action && mods == GLFW_MOD_SHIFT) {translation[1] -= 0.001f * Height_global; break;}
            else if (mods != GLFW_MOD_SHIFT) {rotationX += 4;  break;}
            break;
        case GLFW_KEY_F:
            if (action && mods == GLFW_MOD_SHIFT) auto_strech = !auto_strech; break;
        case GLFW_KEY_SPACE: break;
        case GLFW_KEY_S:
          if(action == GLFW_RELEASE) {
          glGetIntegerv(GL_SHADE_MODEL, params);
          if ( params[0] == GL_FLAT) {
            //printf("Switched to Smooth\n");
            glShadeModel(GL_SMOOTH);
            break;
          } else {
            //printf("Switched to Flat\n");
            glShadeModel(GL_FLAT);
            break;
          }
          break;
          }
        case GLFW_KEY_W:
          if (action == GLFW_RELEASE) {
          glGetIntegerv(GL_POLYGON_MODE, params);
          if (params[0] == GL_FILL) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            break;
          } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            break;
          }
          break;
        }
        default: break;
    }

}
/**
We are tessellating a surface, done in 2 days! LET"S DO IT!!!!
Subdivision
**/
vector<float> add(vector<float> a, vector<float> b) {
  vector<float> result = {a[0] + b[0], a[1] + b[1], a[2] + b[2]};
  return result;
}

float dist(vector<float> a, vector<float> b) {
	float xd = a[0] - b[0];
	float yd = a[1] - b[1];
	float zd = a[2] - b[2];
	return (float) sqrt(xd * xd + yd * yd + zd * zd);
}

vector<float> findPoint(vector<float> intercept, vector<float> slope, float t) {
  float xDiff = t * slope[0];
  float yDiff = t * slope[1];
  float zDiff = t * slope[2];
  vector<float> result = {intercept[0] + xDiff, intercept[1] + yDiff, intercept[2] + zDiff};
  return result;
}

vector<float> scale(vector<float> a, float c) {
  float x = c * a[0];
  float y = c * a[1];
  float z = c * a[2];
  vector<float> result = {x, y, z};
  return result;
}

vector<float> subtract(vector<float> a, vector<float> b) {
  float x = a[0] - b[0];
  float y = a[1] - b[1];
  float z = a[2] - b[2];
  vector<float> result = {x, y, z};
  return result;
}

int vectorEquality(vector<float> a, vector<float> b) {
  float xDiff = a[0] - b[0];
  float yDiff = a[1] - b[1];
  float zDiff = a[2] - b[2];
  if (xDiff == 0 && yDiff == 0 && zDiff == 0) {
    return 0;
  }
  int x = 1;
  int y = 2;
  int z = 3;
  return x + y + z;
}

vector<float> cross(vector<float> a, vector<float> b) {
  vector<float> result = {a[1] * b[2] - a[2] * b[1],
  a[2] * b[0] - a[0] * b[2],
  a[0] * b[1] - a[1] * b[0]};
  return result;
}

vector<float> normalize(vector<float> a) {
  float length = (float) sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
  vector<float> result = {a[0] / length, a[1] / length, a[2] / length};
  return result;
}

vector<vector<float>> bezierCurveInterpolate(vector<vector<float>> controls, float u) {
  // Create control curves from patch control points
  // Has 4 sets of 3 floats Credits to algorithm provided by cs184 course notes Spring 2016
  float v = 1.0f - u;
  vector<float> a = add(scale(controls[0], v), scale(controls[1], u));
  vector<float> b = add(scale(controls[1], v), scale(controls[2], u));
  vector<float> c = add(scale(controls[2], v), scale(controls[3], u));
  vector<float> d = add(scale(a, v), scale(b, u));
  vector<float> e = add(scale(b, v), scale(c, u));
  vector<float> p = add(scale(d, v), scale(e, u));
  vector<float> pDiff = scale(subtract(e, d), 3.0f);
  vector<vector<float>> result(2, vector<float>(3));
  result[0] = p;
  result[1] = pDiff;
  return result;

}
vector<vector<float>> bezierPatchInterpolate(vector<vector<vector<float>>> patch, float u, float v) {
  // Calls bezierPatchInterpolate
  vector<vector<float>> uCurve(4, vector<float>(3));
  vector<vector<float>> vCurve(4, vector<float>(3));
  vCurve[0] = bezierCurveInterpolate(patch[0], u)[0];
  vCurve[1] = bezierCurveInterpolate(patch[1], u)[0];
  vCurve[2] = bezierCurveInterpolate(patch[2], u)[0];
  vCurve[3] = bezierCurveInterpolate(patch[3], u)[0];
  vector<vector<float>> v0 = {patch[0][0], patch[1][0], patch[2][0], patch[3][0]};
  vector<vector<float>> v1 = {patch[0][1], patch[1][1], patch[2][1], patch[3][1]};
  vector<vector<float>> v2 = {patch[0][2], patch[1][2], patch[2][2], patch[3][2]};
  vector<vector<float>> v3 = {patch[0][3], patch[1][3], patch[2][3], patch[3][3]};
  uCurve[0] = bezierCurveInterpolate(v0, v)[0];
  uCurve[1] = bezierCurveInterpolate(v1, v)[0];
  uCurve[2] = bezierCurveInterpolate(v2, v)[0];
  uCurve[3] = bezierCurveInterpolate(v3, v)[0];

  vector<vector<float>> vResult = bezierCurveInterpolate(vCurve, v);
  vector<vector<float>> uResult = bezierCurveInterpolate(uCurve, u);
  vector<float> xprod = cross(uResult[1], vResult[1]);
  // int errCode;
  // if ((errCode = vectorEquality(vResult[0], uResult[1])) != 0) {
  //   printf("Error %d\n", errCode);
  // }
  vector<vector<float>> results(2, vector<float>(3));
  results[0] = uResult[0];
  results[1] = normalize(xprod);
  return results;
}

vector<vector<vector<float>>> subdividePatch(vector<vector<vector<float>>> patch, float step) {
  int numDiv = (int) floor((1 + epsilon) /step);
  int uIndex;
  int vIndex;
  float u, v;
  vector<vector<vector<float>>> pResults (numDiv * numDiv, vector<vector<float>>(2, vector<float>(3)));
  for (uIndex = 0; uIndex < numDiv; uIndex ++ ) {
    u = uIndex * step;

    for (vIndex = 0; vIndex < numDiv; vIndex++ ) {
      v = vIndex * step;
      pResults[uIndex * numDiv + vIndex] = bezierPatchInterpolate(patch, u, v);

    }
  }
  return pResults;
}

int midpointTest(vector<float> mid, vector<float> curveMid) {
	float d = dist(mid, curveMid);
	if (d <= step) {
		return 1;
	}
	return 0;
}
vector<float> findMid2f(vector<float> a, vector<float> b) {
	float x = (a[0] + b[0]) * 0.5f;
	float y = (a[1] + b[1]) * 0.5f;
	if (x > 1.0f) {
		x = 1.0f;
	}
	if (x < 0.0f) {
		x = 0.0f;
	}
	if (y > 1.0f) {
		y = 1.0f;
	}
	if (y < 0.0f) {
		y = 0.0f;
	}
	vector<float> result = { x, y };
	return result;
}
void drawTriangle(vector<vector<vector<float>>> patch, vector<vector<float>> t, vector<vector<float>> uv) {
	vector<float> mid0 = add(scale(t[0], 0.5f), scale(t[1], 0.5f));
	vector<float> mid1 = add(scale(t[1], 0.5f), scale(t[2], 0.5f));
	vector<float> mid2 = add(scale(t[2], 0.5f), scale(t[0], 0.5f));
	vector<float> midUV0 = findMid2f(uv[0], uv[1]);
	vector<float> midUV1 = findMid2f(uv[1], uv[2]);
	vector<float> midUV2 = findMid2f(uv[2], uv[0]);
	vector<vector<float>> mid0Values = bezierPatchInterpolate(patch, midUV0[0], midUV0[1]);
	vector<vector<float>> mid1Values = bezierPatchInterpolate(patch, midUV1[0], midUV1[1]);
	vector<vector<float>> mid2Values = bezierPatchInterpolate(patch, midUV2[0], midUV2[1]);
	//vector<float> midReal0 = mid0Values[0];
	//vector<float> midReal1 = mid1Values[0];
	//vector<float> midReal2 = mid2Values[0];
	vector<float> midReal0 = bezierPatchInterpolate(patch, midUV0[0], midUV0[1])[0];
	vector<float> midReal1 = bezierPatchInterpolate(patch, midUV1[0], midUV1[1])[0];
	vector<float> midReal2 = bezierPatchInterpolate(patch, midUV2[0], midUV2[1])[0];

	int e0 = midpointTest(mid0, midReal0);
	int e1 = midpointTest(mid1, midReal1);
	int e2 = midpointTest(mid2, midReal2);
	if (e0 + e1 + e2 == 3) {
		vector<float> n0 = bezierPatchInterpolate(patch, uv[0][0], uv[0][1])[1];
		vector<float> n1 = bezierPatchInterpolate(patch, uv[1][0], uv[1][1])[1];
		vector<float> n2 = bezierPatchInterpolate(patch, uv[2][0], uv[2][1])[1];
		glBegin(GL_TRIANGLES);
		glNormal3f(n0[0], n0[1], n0[2]);
		//glNormal3f(mid0Values[1][0], mid0Values[1][1], mid0Values[1][2]);
		glVertex3f(t[0][0], t[0][1], t[0][2]);
		glNormal3f(n1[0], n1[1], n1[2]);
		//glNormal3f(mid1Values[1][0], mid1Values[1][1], mid1Values[1][2]);
		glVertex3f(t[1][0], t[1][1], t[1][2]);
		glNormal3f(n2[0], n2[1], n2[2]);
		//glNormal3f(mid2Values[1][0], mid2Values[1][1], mid2Values[1][2]);
		glVertex3f(t[2][0], t[2][1], t[2][2]);
		glEnd();
		vector<vector<float>> ns = { n0, n1, n2 };
		allNormals.push_back(ns);
		allTriangles.push_back(t);
	}
	else {
		if (!e0 && !e1 && !e2) {
			// 4 triangles to make
			vector<vector<float>> t1 = { t[0], midReal0, midReal2 };
			vector<vector<float>> t2 = { midReal0, t[1], midReal1 };
			vector<vector<float>> t3 = { midReal2, midReal1, t[2] };
			vector<vector<float>> t4 = { midReal0, midReal1, midReal2 };
			vector<vector<float>> uv1 = { uv[0], midUV0, midUV2 };
			vector<vector<float>> uv2 = { midUV0, uv[1], midUV1 };
			vector<vector<float>> uv3 = { midUV2, midUV1, uv[2] };
			vector<vector<float>> uv4 = { midUV0, midUV1, midUV2 };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
			drawTriangle(patch, t3, uv3);
			drawTriangle(patch, t4, uv4);
		}
		else if (!e0 && !e1) {
			// 3 triangles to make
			vector<vector<float>> t1 = { t[0], midReal0, t[2] };
			vector<vector<float>> t2 = { midReal0, midReal1, t[2] };
			vector<vector<float>> t3 = { midReal0, t[1], midReal1 };
			vector<vector<float>> uv1 = { uv[0], midUV0, uv[2] };
			vector<vector<float>> uv2 = { midUV0, midUV1, uv[2] };
			vector<vector<float>> uv3 = { midUV0, uv[1], midUV1 };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
			drawTriangle(patch, t3, uv3);
		}
		else if (!e1 && !e2) {
			// 3 triangles
			vector<vector<float>> t1 = { t[0], t[1], midReal1 };
			vector<vector<float>> t2 = { t[0], midReal1, midReal2 };
			vector<vector<float>> t3 = { midReal1, midReal2, t[2] };
			vector<vector<float>> uv1 = { uv[0], uv[1], midUV1 };
			vector<vector<float>> uv2 = { uv[0], midUV1, midUV2 };
			vector<vector<float>> uv3 = { midUV1, midUV2, uv[2] };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
			drawTriangle(patch, t3, uv3);
		}
		else if (!e0 && !e2) {
			// 3 trinalges
			vector<vector<float>> t1 = { midReal2, t[1], t[2] };
			vector<vector<float>> t2 = { midReal0, t[1], midReal2 };
			vector<vector<float>> t3 = { t[0], midReal0, midReal2 };
			vector<vector<float>> uv1 = { midUV2, uv[1], uv[2] };
			vector<vector<float>> uv2 = { midUV0, uv[1], midUV2 };
			vector<vector<float>> uv3 = { uv[0], midUV0, midUV2 };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
			drawTriangle(patch, t3, uv3);
			
		}
		else if (!e0) {
			// 2 triangles t[0],mt0,t[2] / mt0,t[1], t[2]
			vector<vector<float>> t1 = { t[0], midReal0, t[2] };
			vector<vector<float>> t2 = { midReal0, t[1], t[2] };
			vector<vector<float>> uv1 = { uv[0], midUV0, uv[2] };
			vector<vector<float>> uv2 = { midUV0, uv[1], uv[2] };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
		}
		else if (!e1) {
			// 2 triangles
			vector<vector<float>> t1 = { t[0], t[1], midReal1 };
			vector<vector<float>> t2 = { t[0], midReal1, t[2] };
			vector<vector<float>> uv1 = { uv[0], uv[1], midUV1 };
			vector<vector<float>> uv2 = { uv[0], midUV1, uv[2] };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
		}
		else if (!e2) {
			// 2 triangles
			vector<vector<float>> t1 = { t[0], t[1], midReal2 };
			vector<vector<float>> t2 = { midReal2, t[1], t[2] };
			vector<vector<float>> uv1 = { uv[0], uv[1], midUV2 };
			vector<vector<float>> uv2 = { midUV2, uv[1], uv[2] };
			drawTriangle(patch, t1, uv1);
			drawTriangle(patch, t2, uv2);
		}
		else {
			printf("Error: Should not be possible e0=%d e1=%d e2=%d\n", e0, e1, e2);
			return;
		}
	}
}

void subdivideAdapt(vector<vector<vector<float>>> patch) {
	vector<vector<float>> tri1 = { patch[0][0], patch[3][3], patch[0][3] };
	vector<vector<float>> tri2 = { patch[0][0], patch[3][0], patch[3][3] };
	vector<float> uv10 = { 0.0f, 0.0f };
	vector<float> uv11 = { 1.0f, 1.0f };
	vector<float> uv12 = { 1.0f, 0.0f };
	vector<float> uv20 = { 0.0f, 0.0f };
	vector<float> uv21 = { 0.0f, 1.0f };
	vector<float> uv22 = { 1.0f, 1.0f };
	vector<vector<float>> uv1 = { uv10, uv11, uv12 };
	vector<vector<float>> uv2 = { uv20, uv21, uv22 };
	drawTriangle(patch, tri1, uv1);
	drawTriangle(patch, tri2, uv2);

}



//****************************************************
// Draw a cube. You don't need this for your final assignment, but it's
// here so you don't look at a blank screen.
// Taken from https://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_Examples.html
//****************************************************
void drawCube() {
   glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads
      // Top face (y = 1.0f)
      // Define vertices in counter-clockwise (CCW) order with normal pointing out
      glColor3f(0.0f, 1.0f, 0.0f);     // Green
      glVertex3f( 1.0f, 1.0f, -1.0f);
      glVertex3f(-1.0f, 1.0f, -1.0f);
      glVertex3f(-1.0f, 1.0f,  1.0f);
      glVertex3f( 1.0f, 1.0f,  1.0f);

      // Bottom face (y = -1.0f)
      glColor3f(1.0f, 0.5f, 0.0f);     // Orange
      glVertex3f( 1.0f, -1.0f,  1.0f);
      glVertex3f(-1.0f, -1.0f,  1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f( 1.0f, -1.0f, -1.0f);

      // Front face  (z = 1.0f)
      glColor3f(1.0f, 0.0f, 0.0f);     // Red
      glVertex3f( 1.0f,  1.0f, 1.0f);
      glVertex3f(-1.0f,  1.0f, 1.0f);
      glVertex3f(-1.0f, -1.0f, 1.0f);
      glVertex3f( 1.0f, -1.0f, 1.0f);

      // Back face (z = -1.0f)
      glColor3f(1.0f, 1.0f, 0.0f);     // Yellow
      glVertex3f( 1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f,  1.0f, -1.0f);
      glVertex3f( 1.0f,  1.0f, -1.0f);

      // Left face (x = -1.0f)
      glColor3f(0.0f, 0.0f, 1.0f);     // Blue
      glVertex3f(-1.0f,  1.0f,  1.0f);
      glVertex3f(-1.0f,  1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f,  1.0f);

      // Right face (x = 1.0f)
      glColor3f(1.0f, 0.0f, 1.0f);     // Magenta
      glVertex3f(1.0f,  1.0f, -1.0f);
      glVertex3f(1.0f,  1.0f,  1.0f);
      glVertex3f(1.0f, -1.0f,  1.0f);
      glVertex3f(1.0f, -1.0f, -1.0f);
   glEnd();  // End of drawing color-cube
}


void drawBezier(vector<vector<vector<vector<float>>>> patches, int adapt) {
	if (rendered) {
		if (adapt) {
			for (int m = 0; m < allTriangles.size(); m++) {
				vector<vector<float>> t = allTriangles[m];
				vector<vector<float>> tn = allNormals[m];
				glBegin(GL_TRIANGLES);
				glNormal3f(tn[0][0], tn[0][1], tn[0][2]);
				glVertex3f(t[0][0], t[0][1], t[0][2]);
				glNormal3f(tn[1][0], tn[1][1], tn[1][2]);
				glVertex3f(t[1][0], t[1][1], t[1][2]);
				glNormal3f(tn[2][0], tn[2][1], tn[2][2]);
				glVertex3f(t[2][0], t[2][1], t[2][2]);
				glEnd();
			}
		}
		else {
			for (int m = 0; m < allQuads.size(); m++) {
				vector<vector<float>> vs = allQuads[m];
				vector<vector<float>> ns = allNormals[m];
				glBegin(GL_QUADS);
				glNormal3f(ns[0][0], ns[0][1], ns[0][2]);
				glVertex3f(vs[0][0], vs[0][1], vs[0][2]);
				glNormal3f(ns[1][0], ns[1][1], ns[1][2]);
				glVertex3f(vs[1][0], vs[1][1], vs[1][2]);
				glNormal3f(ns[2][0], ns[2][1], ns[2][2]);
				glVertex3f(vs[2][0], vs[2][1], vs[2][2]);
				glNormal3f(ns[3][0], ns[3][1], ns[3][2]);
				glVertex3f(vs[3][0], vs[3][1], vs[3][2]);
				glEnd();
			}
		}
		return;
	}
		int numDiv = (int)floor((1 + epsilon) / step);
		rendered = 1;
		if (adapt == 0) {
			//Uniform
			for (unsigned int i = 0; i < patches.size(); i++) {
				vector<vector<vector<float>>> pResults = subdividePatch(patches[i], step);
				for (unsigned int j = 0; j < numDiv - 1; j++) {
					for (unsigned int k = 0; k < numDiv - 1; k++) {
						vector<float> v0 = pResults[(j + 1) * numDiv + k][0];
						vector<float> v1 = pResults[j * numDiv + k][0];
						vector<float> v2 = pResults[j * numDiv + k + 1][0];
						vector<float> v3 = pResults[(j + 1) * numDiv + k + 1][0];
						vector<float> n0 = pResults[(j + 1) * numDiv + k][1];
						vector<float> n1 = pResults[j * numDiv + k][1];
						vector<float> n2 = pResults[j * numDiv + k + 1][1];
						vector<float> n3 = pResults[(j + 1) * numDiv + k + 1][1];
						vector<vector<float>> quad = { v0, v1, v2, v3 };
						vector<vector<float>> normals = { n0, n1, n2, n3 };
						allQuads.push_back(quad);
						allNormals.push_back(normals);
						glBegin(GL_QUADS);
						glNormal3f(n0[0], n0[1], n0[2]);
						glVertex3f(v0[0], v0[1], v0[2]);
						glNormal3f(n1[0], n1[1], n1[2]);
						glVertex3f(v1[0], v1[1], v1[2]);
						glNormal3f(n2[0], n2[1], n2[2]);
						glVertex3f(v2[0], v2[1], v2[2]);
						glNormal3f(n3[0], n3[1], n3[2]);
						glVertex3f(v3[0], v3[1], v3[2]);
						glEnd();
					}
				}
			}
		}
		else {
			//Adaptive
			for (int i = 0; i < patches.size(); i++) {
				subdivideAdapt(patches[i]);
			}
		}
	}
//****************************************************
// function that does the actual drawing of stuff
//***************************************************

void displayCurve(GLFWwindow * window) {
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f ); //clear background screen to black
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                // clear the color buffer (sets everything to black)
    glLoadIdentity();                            // make sure transformation is "zero'd"
    glPushMatrix();
	glScalef(scaling, scaling, scaling);
    glTranslatef (translation[0], translation[1], translation[2]);
    glRotatef(rotationX, 1.0f, 0.0f, 0.0f );
    glRotatef(rotationY, 0.0f, 1.0f, 0.0f);
    drawBezier(allPatches, adapt);
    glPopMatrix();
    glfwSwapBuffers(window);

}
void display( GLFWwindow* window)
{
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f ); //clear background screen to black

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                // clear the color buffer (sets everything to black)
    glMatrixMode(GL_MODELVIEW);                  // indicate we are specifying camera transformations
    glLoadIdentity();                            // make sure transformation is "zero'd"

    //----------------------- code to draw objects --------------------------
    glPushMatrix();
    glTranslatef (translation[0], translation[1], translation[2]);
    glRotatef(45, 1, 1, 0); //rotates the cube below
    drawCube(); // REPLACE ME!

    glPopMatrix();

    glfwSwapBuffers(window);

    // note: check out glPolygonMode and glShadeModel
    // for wireframe and shading commands

}

// void display

//****************************************************
// function that is called when window is resized
//***************************************************
void size_callback(GLFWwindow* window, int width, int height)
{
    // Get the pixel coordinate of the window
    // it returns the size, in pixels, of the framebuffer of the specified window
    glfwGetFramebufferSize(window, &Width_global, &Height_global);

    glViewport(0, 0, Width_global, Height_global);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, Width_global, 0, Height_global, 1, -1);

    displayCurve(window);
}

vector<vector<float>> getPatch(char *str) {
  vector<vector<float>> result(4, vector<float>(3));
  float f[12];
  string s(str);
  stringstream ss(s);
  for (int i=0; i<12; i++) {
    ss >> f[i];
  }
  for (int j = 0; j < 4 ; j++) {
    vector<float> fv = {f[j * 3], f[j * 3 + 1], f[j * 3 + 2]};
    result[j] = fv;
  }

  return result;

}

vector<vector<vector<vector<float>>>> getPatches(FILE *fp, int numPatches) {
  char str[200];
  int patchLines = 0; // Increment to 4 then push_back new patchLines
  int patch_i = 0;
  vector<vector<vector<vector<float>>>> patches(numPatches, vector<vector<vector<float>>>(4, vector<vector<float>>(4, vector<float>(3))));
  while (fgets(str, 200, fp) != NULL) {
    // 5 is an arbitrary count to make sure we aren't reading in "\r\n" or some amalgam of that silliness
    if (strlen(str) > 5) {
      // if patchlines == 4 then reset patchLines, and pushback to patches the patch
        vector<vector<float>> line = getPatch(str);
        for (int i = 0; i < 4; i++ ) {
          for (int j = 0; j < 3; j++) {
            patches.at(patch_i).at(patchLines).at(i)[j] = line[i][j];
          }
        }
        patchLines++;
        if (patchLines == 4) {
          patchLines = 0;
          patch_i++;
          if (patch_i == numPatches) {
            return patches;
          }
        }
    }
  }
  return patches;
}

//****************************************************
// the usual stuff, nothing exciting here
//****************************************************
int main(int argc, char *argv[]) {
  if (argc != 3 && argc != 4) {
    cerr << "Usage: ./as3 inputfile.bez subdivision" << endl;
    return -1;
  }
  char *filename = argv[1];
  step = (float) atof(argv[2]);
  if (argc == 4) {
    if (strcmp(argv[3], "-a") == 0) {
      adapt = 1;
    }
  }
  FILE *fp = fopen(filename, "r");
  char str[200];
  fgets(str, 200, fp);
  int numPatches = atoi(str);
  allPatches = getPatches(fp, numPatches);
  fclose(fp);
    //This initializes glfw
  initializeRendering();

  GLFWwindow* window = glfwCreateWindow( Width_global, Height_global, "CS184", NULL, NULL );
  if ( !window )
  {
      cerr << "Error on window creating" << endl;
      glfwTerminate();
      return -1;
  }

  const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
  if ( !mode )
  {
      cerr << "Error on getting monitor" << endl;
      glfwTerminate();
      return -1;
  }

  glfwMakeContextCurrent( window );

  // Get the pixel coordinate of the window
  // it returns the size, in pixels, of the framebuffer of the specified window
  glfwGetFramebufferSize(window, &Width_global, &Height_global);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-3.5, 3.5, -3.5, 3.5, 5, -5);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_DEPTH_TEST);  // enable z-buffering
  glDepthFunc(GL_LESS);

  glfwSetWindowTitle(window, "CS184");
  glfwSetWindowSizeCallback(window, size_callback);
  glfwSetKeyCallback(window, key_callback);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glEnable(GL_LIGHTING);
  glEnable(GL_NORMALIZE);
  GLfloat diffuse[] = {0.61424f, 0.04136f, 0.04136f, 1.0f};
  GLfloat ambient[] = {0.1745f, 0.01175f, 0.01175f, 1.0f};
  GLfloat specular[] = {0.727811f, 0.626959f, 0.626959f, 1.0f};
  GLfloat lighPosition[] = {0.0f, 0.0f, 3.0f, 1.0f};
  GLfloat lp[] = { 3.0f, 3.0f, 3.0f, 1.0f };
  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);
  glLightfv(GL_LIGHT0, GL_POSITION, lighPosition);
  glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
  glLightfv(GL_LIGHT1, GL_POSITION, lp);
  glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
  glLightfv(GL_LIGHT1, GL_SPECULAR, specular);
    while( !glfwWindowShouldClose( window ) ) // infinite loop to draw object again and again
    {   // because once object is draw then window is terminated

        displayCurve( window );

        if (auto_strech){
            glfwSetWindowSize(window, mode->width, mode->height);
            glfwSetWindowPos(window, 0, 0);
        }

        glfwPollEvents();

    }

    return 0;
}
